<div id="cookie-consent-banner">
	<div id="cookie-consent-main-text" style="text-align: center; width:100%; background:<?php print $cookie_consent_banner_bgcolor ?>; border: 1px solid #003768;">
		<?php print $cookie_consent_banner_content ?>
		<br />
		<input type="checkbox" id="cookie-consent-acceptance-checkbox" />
		I accept cookies from this site.
		<input type="button" class="button" id="cookie-consent-accept" value="Continue" />
		<div style="display:none; color:#b20001" id="error">
			<strong>Please select I accept cookies checkbox</strong>
		</div>
	</div>
	<div id="cookie-consent-extended-text" style="display:none; background:#E9E9E9">
		<strong>This cookie will only contain a value designed to confirm your acceptance and nothing more. It is only used to store your preference for this site. Storing your preference permanently will simply stop you being asked this question each time you visit the site but will not effect your ability to use it.</strong>
	</div>
</div>
