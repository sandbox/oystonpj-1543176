Drupal.behaviors.cookie_consent = function(context) {  
  html = Drupal.settings.cookie_consent.banner_content;  
  consent = Drupal.settings.cookie_consent.consent;
  cookie_name = Drupal.settings.cookie_consent.cookie_name;
  request_cookie_permission(html, consent, cookie_name); 
}

function request_cookie_permission(html, consent, cookie_name) {
	if(!consent) {
		var banner = $(html)
			.attr({ "id": "cookie-consent-banner" })
			.css({ "top": "-1", "position": "relative", "z-index": "100" })
			.height('50')
			.width('100%')
			.hide()
			.prependTo('body');
			
		$('#cookie-consent-accept').click(function() {
			var checkbox = $('#cookie-consent-acceptance-checkbox').attr('checked');
			if(!checkbox) {
				$('#cookie-consent-extended-text').show();
			} else {
				cookie_consent_agreed(cookie_name);
				banner.slideUp();
			}
		});
		banner.slideDown();		
	}
}

function cookie_consent_agreed(cookie_name) { 
	var date = new Date(); 
	var agreed_date_string = date.toUTCString();
	date.setDate(date.getDate() + 365);
	document.cookie = cookie_name +"=" +agreed_date_string +";expires=" + date.toUTCString() + ";path=/";
}
