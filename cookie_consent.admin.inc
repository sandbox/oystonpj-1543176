<?php

/**
 * @file
 * Admin page callbacks for the cookie_consent module.
 */

/**
 * Menu callback: Display the salt module settings form.
 */
function cookie_consent_settings_form(&$form_state) {
  $form['cookie_consent'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
  );

  $form['cookie_consent']['cookie_consent_required'] = array(
    '#type' => 'radios',
    '#title' => t('Require Consent'),
    '#default_value' => variable_get('cookie_consent_required', 0),
    '#required' => TRUE,
	'#options' => array(0 => t('No'), 1 => t('Yes')),
    '#description' => t('If YES then all visitors must explicitly give permission for cookies to be used by the site, if NO then permission is implied without asking the site visitor')
  );

  $form['cookie_consent']['cookie_consent_banner_content'] = array(
    '#type' => 'textarea',
    '#title' => t('Cookie Consent Banner Content'),
    '#default_value' => variable_get('cookie_consent_banner_content', '<strong>This site would like to place cookies on your computer to help us make this website better. To find out more about the cookies, see our <a href="/privacy-policy">privacy notice</a></strong>'),
    '#cols' => 60,
    '#required' => FALSE,
    '#description' => t('This is the message displayed to visitors when requesting their permission to use cookies on the website')
  );

  $form['cookie_consent']['cookie_consent_banner_bgcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Consent banner background color'),
    '#default_value' => variable_get('cookie_consent_banner_bgcolor', '#FDD017'),
    '#size' => 10,
	'#maxlength' => 7,
    '#required' => FALSE,
    '#description' => t('The background color used when displaying the consent banner')
  );

  $form['cookie_consent_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
	'#collapsed' => TRUE
  );

  $form['cookie_consent_advanced']['cookie_consent_cookie_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Consent Cookie Name'),
    '#default_value' => variable_get('cookie_consent_cookie_name', 'cookie-consent-given'),
    '#size' => 40,
	'#maxlength' => 35,
    '#required' => TRUE,
    '#description' => t('The name of the cookie stored in the site visitors browser.  WARNING: Changing this setting will reset consent acceptance for all site visitors')
  );  

  return system_settings_form($form);
}

/**
 * Validate the cookie_consent settings form submission.
 */
function cookie_consent_settings_form_validate($form, &$form_state) {
  /* Ensure consent banner background color is specified as a HTML hex code #XXXXXX */
  if(!preg_match('/^\#([0-9a-f]{1,2}){3}$/i', $form_state['values']['cookie_consent_banner_bgcolor'])) {
	form_set_error('cookie_consent_banner_bgcolor', t('background colour should be a HTML color code #XXXXXX'));
  }
  if(!preg_match('/^[A-Za-z_-]*$/', $form_state['values']['cookie_consent_cookie_name'])) {
    form_set_error('cookie_consent_cookie_name', t('consent cookie can only contain letters, underscore or dash characters'));
  }
}
